# OCTOPI
The code behind Bryce and the freshmen's bot.

## License
This project is licensed under the GNU GPLv3 license. See [LICENSE](LICENSE).
